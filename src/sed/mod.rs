use std::process::Command;
use std::str;

pub fn s(arg: &str, edit: &str, file: &str) {
    let sf = Command::new("sed")
        .arg(&arg)
        .arg(&edit)
        .arg(&file)
        .output()
        .expect("Failed to edit file");
    sf.stdout;
}
